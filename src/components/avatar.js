import React, { Component } from 'react';
import { StyleSheet, Image, Text, View } from 'react-native';
import PropTypes from 'prop-types';

import { COLORS } from '@lib/theme';

class Avatar extends Component {
    static propTypes = {
        borderWidth: PropTypes.number.isRequired,
        size: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        source: PropTypes.string
    };

    constructor(props) {
        super(props);
    }

    render() {
        const { borderWidth, name, size, source } = this.props;
        const dynamicStylesAvatar = {
            borderRadius: Math.floor(size / 2),
            borderWidth,
            height: size,
            width: size
        };
        if (source) {
            avatar = <Image style={[styles.avatar, dynamicStylesAvatar]} source={{ uri: source }} />;
        } else {
            const dynamicStylesInitials = { fontSize: Math.floor(size / 2.5) };
            avatar = <View style={[styles.avatar, dynamicStylesAvatar]}>
                <Text style={[styles.initials, dynamicStylesInitials]}>{name}</Text>
            </View>;
        }

        return (
            <View style={styles.container}>
                {avatar}
            </View>);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    avatar: {
        justifyContent: 'center',
        backgroundColor: COLORS.avatarBackgroundColor,
        borderColor: COLORS.primary,
        position: 'absolute',
        top: 0,
        left: 0
    },
    initials: {
        color: COLORS.darkText,
        textAlign: 'center',
        fontWeight: 'bold',
    }

});

export { Avatar };
